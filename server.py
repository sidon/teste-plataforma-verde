#!/usr/bin/env python
"""
Main module of the server file
"""

# 3rd party moudles
from flask import redirect, render_template

# Local modules
import config

# Get the application instance
connex_app = config.connex_app

# Read the swagger.yml file to configure the endpoints
connex_app.add_api("swagger.yml")

# Create a URL route in our application for "/rest"
@connex_app.route("/rest")
def home():
    """
    This function just responds to the browser URL
    localhost:5000/
    """
    return redirect("/api/ui", code=302)


@connex_app.route("/")
def index():
    """
    This function just responds to the browser URL
    localhost:5000/
    """
    return render_template("readme.html")


@connex_app.route("/readme")
def readme():
    """
    This function just responds to the browser URL
    localhost:5000/
    """
    return render_template("readme.html")


@connex_app.route("/futebol")
def futebol():
    """
    This function just responds to the browser URL
    localhost:5000/
    """
    return render_template("futebol.html")

@connex_app.route("/queries")
def queries():
    """
    This function just responds to the browser URL
    localhost:5000/
    """
    return render_template("qrys.html")


if __name__ == "__main__":
    connex_app.run(debug=True)