#######################################################
``Teste para desenvolvimento Python Plataforma Verde``
#######################################################


Resumo
*******
| O teste consiste em desenvolver uma api RESTful simples e Query SQL básicas.

TL;DR
*******
| A aplicação está hospedada no Heroku http://www.heroku.com.
| Para testá-la clique: https://sdn-pverde.herokuapp.com.
| Repositorio no gitlab: https://gitlab.com/sidon/teste-plataforma-verde.
| Documentação REST API: https://sdn-pverde.herokuapp.com/api/ui/

Descrição do desafio
*********************
1) Para monitorar o desempenho dos times em um campeonato de futebol, crie uma
classe Time, que tem como atributos o número de pontos, gols prós, gols contras e o
número de partidas jogados. Dentro desta classe, defina métodos para:

a) calcular a média de gols por partida;
b) calcular a média de pontos por partida;
c) calcular o saldo de gols do time;
d) atualizar os atributos do time de acordo com os resultados de uma partida \(vitória/derrota/empate, gols feitos e gols sofridos\);
e) retornar os seguintes status do time: partidas jogadas, pontos feitos, saldo de gols, média de gols por partida, média de pontos por partida

  OBS: vitória vale 3 pontos, empate vale 1 ponto e derrota não vale nada

    Link da solução:
        https://sdn-pverde.herokuapp.com/querys

2) Considere as seguintes tabelas em um banco SQL:

Compras

+------+--------------+--------+----------------+
| id   | id_usuario   | valor  | item           |
+======+==============+========+================+
| 1    | 1            | 500    | Colchão        |
+------+--------------+--------+----------------+
| 2    | 2            | 250    | Jogo de Pratos |
+------+--------------+--------+----------------+
| 3    | 2            | 1000   | Celular        |
+------+--------------+--------+----------------+

Usuários

+------+--------------+--------+---------------------+
| id   | nome         | valor  | email               |
+======+==============+========+=====================+
| 1    | João         | 500    |  joao@email.com.br  |
+------+--------------+--------+---------------------+
| 2    | Pedro        | 250    |  pedro@email.com.br |
+------+--------------+--------+---------------------+
| 3    | Maria        | 1000   |  maria@email.com.br |
+------+--------------+--------+---------------------+


Criar consultas SQL que retornem:

a)

+-----------+--------+----------+---------+----------------+
|id_usuario | nome   |id_compra |   valor | item           |
+===========+========+==========+=========+================+
|         1 | João   |        1 |     500 | Colchão        |
+-----------+--------+----------+---------+----------------+
|         2 | Maria  |        2 |     250 | Jogo de Pratos |
+-----------+--------+----------+---------+----------------+
|         2 | Maria  |        3 |    1000 | Celular        |
+-----------+--------+----------+---------+----------------+

b)

+-----------+--------+----------+---------+----------------+
|id_usuario | nome   |id_compra |   valor | item           |
+===========+========+==========+=========+================+
|         1 | João   |        1 |     500 | Colchão        |
+-----------+--------+----------+---------+----------------+
|         2 | Maria  |        2 |     250 | Jogo de Pratos |
+-----------+--------+----------+---------+----------------+
|         2 | Maria  |        3 |    1000 | Celular        |
+-----------+--------+----------+---------+----------------+
|         3 | Pedro  |      Null|    Null | Null           |
+-----------+--------+----------+---------+----------------+


  Observação:

  A não ser que isso faça parte do teste, é possível notar um pequeno "erro" no enunciado desse tópico.
  Na tabela que apresenta as compras, só existem registros referentes às compras dos usuários cujos id's são 1 e 2,
  ou seja João e Pedro, nas tabelas apresetadas para o resultado esperado das consultas SQL constam registros de compras
  efetuadas pela Maria como se o Id dela fosse o de pedro. Na solução desconsiderei esse erro e construi a query
  correta.


  Link da solução:
     https://sdn-pverde.herokuapp.com/futebol


Ambiente de desenvolvimento:
****************************

+-------------------+---------------------------+------------+
| Resource          | Description               | Version    |
+===================+===========================+============+
| Computer          | Desktop 16 GB Memory      | I5 G5      |
+-------------------+---------------------------+------------+
| Operating System  | Ubuntu  LTS               | 18.04      |
+-------------------+---------------------------+------------+
| Editor/IDE        | Pycharm                   | 2019.3     |
+-------------------+---------------------------+------------+
| venv              | Conda (Miniconda)         | 4.7.12     |
+-------------------+---------------------------+------------+
| Devel Platform    + Django/Python             | 3.8        |
+-------------------+---------------------------+------------+
| CI                | travis-ci                 | 2020       |
+-------------------+---------------------------+------------+
| Coverage          | Pytest --cov              | 2.8.1      |
+-------------------+---------------------------+------------+
| Django            | Main framework            | 3.0.3      |
+-------------------+---------------------------+------------+
| DRF               | dajano-rest-fw            |  4.4.0     |
+-------------------+---------------------------+------------+

Instalação local
****************

Clone o repositorio https://gitlab.com/sidon/teste-plataforma-verde.git;

Crie um ambiente virtual com seu gerenciador favorito (conda, pyenv, virtualenv, etc);

No diretorio clonado, instale os requirements com o comando:

.. code-block::

    $ pip install -r requirements.txt

Crie os dados iniciais com o comando:

.. code-block::

    $ python build_database.py initial_data

Execute a aplicação com o comando:

.. code-block::

    $ python ./server.py

:Date: **08/03/2020**
:Author: **Sidon Duarte**