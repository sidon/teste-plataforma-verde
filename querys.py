from sqlalchemy import create_engine
from sqlalchemy.sql import text

# Local modules
import config

engine = create_engine(config.sqlite_url)

qry = text('''SELECT 
                 c.id_usuario, 
                 c.id, 
                 u.name, 
                 c.item, 
                 c.value 
               FROM 
                 compras AS c
               LEFT OUTER JOIN 
                 usuarios as u
               ON  
                 c.id_usuario = u.id    
               ''')


result1 = engine.execute(qry)

qry = text('''SELECT 
                 u.id, 
                 c.id as id_compra, 
                 u.name, 
                 c.item, 
                 c.value 
               FROM 
                 usuarios AS u
               LEFT OUTER JOIN 
                 compras as c
               ON  
                 u.id = c.id_usuario    
               ''')

result2 = engine.execute(qry)

