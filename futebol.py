#!/usr/bin/env python
import pprint

class Time():
    def __init__(self):
        self.id = 0
        self.pontos = 0
        self.gols_pros = 0
        self.gols_contras = 0
        self.partidas = 0

    def media_gols(self):
        return self.gols_pros / self.partidas

    def saldo_gols(self):
        return self.gols_pros-self.gols_contras

    def atualizar_atributos(self, gols_feitos, gols_sofridos):
        self.partidas+=1
        self.gols_pros+=gols_feitos
        self.gols_contras+=gols_sofridos
        if gols_feitos>gols_sofridos:
            self.pontos += 3
        elif gols_feitos==gols_sofridos:
            self.pontos += 1

    def status(self):
        dict_status = {}
        dict_status['partidas'] = self.partidas
        dict_status['pontos'] = self.pontos
        dict_status['saldo_gols'] = self.saldo_gols()
        return dict_status


if __name__ == "__main__":
    time = Time()
    time.atualizar_atributos(3, 2)
    time.atualizar_atributos(3, 3)
    print('','Resultado inicial: ', time.status(), sep='\n')
    time.atualizar_atributos(5, 4)
    time.atualizar_atributos(2, 6)
    print('','Resultado final: ', time.status(), '', sep='\n')



