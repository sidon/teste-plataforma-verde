from config import db, ma
from sqlalchemy_utils import EmailType

class Person(db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), index=True)
    email = db.Column(EmailType)


class Purchase(db.Model):
    __tablename__ = 'compras'
    id = db.Column(db.Integer, primary_key=True)
    id_usuario = db.Column(db.Integer, db.ForeignKey('usuarios.id'))
    item = db.Column(db.String(32))
    value = db.Column(db.Numeric(), nullable=False)


class PersonSchema(ma.ModelSchema):
    class Meta:
        model = Person
        sqla_session = db.session


class PurchaseSchema(ma.ModelSchema):
    class Meta:
        model = Purchase
        sqla_session = db.session
