#!/usr/bin/env python
import os
from config import db
from models import Person, Purchase

# Data to initialize database with
PEOPLE = (
    {'id': 1, 'name': 'João', 'email': 'joao@email.com.br'},
    {'id': 2, 'name': 'Pedro', 'email': ' pedro@email.com.br'},
    {'id': 3, 'name': 'Maria','email': ' maria@email.com.br'}
)

COMPRAS = (
    {'id_usuario': 1, 'value': 500, 'item': 'Colchão'},
    {'id_usuario': 2, 'value': 250, 'item': 'Jogo de Pratos'},
    {'id_usuario': 2, 'value': 1000, 'item': 'Celular'}
)

# Delete database file if it exists currently
if os.path.exists('green.db'):
    os.remove('green.db')

# Create the database
db.create_all()

# Iterate over the PEOPLE structure and populate the database
for person in PEOPLE:
    p = Person(id=person['id'], name=person['name'], email=person['email'])
    db.session.add(p)

for compra in COMPRAS:
    c = Purchase(id_usuario=compra['id_usuario'], value=compra['value'], item=compra['item'])
    db.session.add(c)

db.session.commit()