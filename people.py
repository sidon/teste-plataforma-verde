"""
This is the people module and supports all the REST actions for the
people data
"""

from flask import make_response, abort
from config import db
from models import Person, PersonSchema


def read_all():
    """
    This function responds to a request for /api/people
    with the complete lists of people

    :return:        json string of list of people
    """
    # Create the list of people from our data
    people = Person.query.order_by(Person.name).all()

    # Serialize the data for the response
    person_schema = PersonSchema(many=True)
    return person_schema.dump(people)


def read_one(id):
    """
    This function responds to a request for /api/people/{person_id}
    with one matching person from people

    :param id:   Id of person to find
    :return:            person matching id
    """
    # Get the person requested
    person = Person.query.filter(Person.id == id).one_or_none()

    # Did we find a person?
    if person:

        # Serialize the data for the response
        person_schema = PersonSchema()
        return person_schema.dump(person)

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            f'Usuário {id}, não encontrado',
        )


def create(person):
    """
    This function creates a new person in the people structure
    based on the passed in person data

    :param person:  person to create in people structure
    :return:        201 on success, 406 on person exists
    """
    name = person.get("name")
    email = person.get("email")

    existing_person = (
        Person.query.filter(Person.email == email).one_or_none()
    )

    # Can we insert this person?
    if existing_person is None:

        # Create a person instance using the schema and the passed in person
        schema = PersonSchema()
        new_person = schema.load(person, session=db.session)

        # Add the person to the database
        db.session.add(new_person)
        db.session.commit()

        # Serialize and return the newly created person in the response
        return schema.dump(new_person), 201

    # Otherwise, nope, person exists already
    else:
        abort(
            409,
            f'O usuário {name} Já existe no sistema',
        )


def update(id, person):
    """
    This function updates an existing person in the people structure
    Throws an error if a person with the name we want to update to
    already exists in the database.

    :param id:   Id of the person to update in the people structure
    :param person:      person to update
    :return:            updated person structure
    """
    # Get the person requested from the db into session
    to_updated_person = Person.query.filter(
        Person.id == id
    ).one_or_none()

    if not to_updated_person:
        abort(
            404,
            f'Não exite usuário com o id {id}',
        )

    # Try to find an existing person with the same informations
    email = person.get("email")
    name = person.get('name')
    existing_person = (
        Person.query.filter(Person.email == email).filter(Person.name== name).one_or_none()
    )

    if existing_person:
        abort(
            400,
            f'Dados do usuário {id} são iguais aos daddos enviados ',
        )


    # turn the passed in person into a db object
    schema = PersonSchema()
    _update = schema.load(person, session=db.session)

    # Set the id to the person we want to update
    _update.person_id = to_updated_person.id

    # merge the new object into the old and commit it to the db
    db.session.merge(_update)
    db.session.commit()

    # return updated person in the response
    return schema.dump(_update), 200


def delete(id):
    """
    This function deletes a person from the people structure

    :param id:   Id of the person to delete
    :return:            200 on successful delete, 404 if not found
    """
    # Get the person requested
    person = Person.query.filter(Person.id == id).one_or_none()

    # Did we find a person?
    if person is not None:
        db.session.delete(person)
        db.session.commit()
        return make_response(
            f'Usuário {id} excluido!', 200
        )

    # Otherwise, nope, didn't find that person
    else:
        abort(
            404,
            f'Usuário {id} não encontrado'
        )